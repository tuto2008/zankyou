$(function(){
	var counters = [];

	$('#fullpage').fullpage({
		verticalCentered: true,
		sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE'],
      	anchors: ['home', 'about', 'spain', 'foryou', 'tools'],
      	menu: '#menu',
      	navigation: true,
		navigationPosition: 'right',
		navigationTooltips: ['home', 'about', 'spain', 'foryou', 'tools'],
		onLeave: function(index, nextIndex, direction){
			if(nextIndex== 1){
				$('#topnav').removeClass('show');
			}
	    },
	    afterLoad: function(anchorLink, index){
			if(index== 1){
				$('#topnav').removeClass('show');
			} else {
				$('#topnav').addClass('show');	
			}
	    },
	    onSlideLeave: function(section, origin, destination, direction){
			for (var k in counters) {
				counters[k].reset();
			}  	
	    },
	    afterSlideLoad: function(section, origin, destination, direction){
	    	if(destination=='data'){
				for (var k in counters) {
					counters[k].start();
				} 
	    	}
	    }
	});

	if($('.counter').length){
		var options = {
			useEasing: true, 
			useGrouping: true, 
			separator: '.', 
			decimal: ',', 
		};
		$('.counter .item').each(function(){
			var id 			= $(this).attr('id');
			var maxCounter 	= $(this).attr('data-max');
			counters.push(new CountUp(id, 0, maxCounter, 0, 2.5, options));
		});
	}

	$('#myVideo').on('ended', function () {
		this.currentTime = 0;
		$.fn.fullpage.moveTo('about');
	});

	$('a.popup').magnificPopup({
		type: 'image'
	});

	$('.nav_pagination a').on('click',function(){
		if( !$(this).hasClass('active') ){
			$('.nav_pagination a, ul.other_tools li').removeClass('active');
			current = $(this).attr('data-item');
			$('ul.other_tools').find('li.'+ current).addClass('active');
			$(this).addClass('active');
		}
		return false;
	});

	$('a.show_video').on('click',function(){
		obj 		= $('.modal .video');
		videoUrl 	= $(this).attr('data-video');
		obj.html('<iframe id="video_01" width="560" height="315" src="'+videoUrl+'?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
		$('.modal').addClass('active');
		return false;
	});
	$('.modal a.close').on('click',function(){
		$('.modal .video').html('');
		$('.modal').removeClass('active');
		return false;
	});
	$('.mobile_menu').on('click',function(){
		if( !$('#topnav').hasClass('open')){
			$('#topnav').addClass('open');
		} else {
			$('#topnav').removeClass('open');
		}
		return false;
	});
	$('#menu a').on('click',function(){
		if( $('#topnav').hasClass('open')){
			$('#topnav').removeClass('open');
		}
	});
	$('.langs a.current').on('click',function(){
		return false;
	});
	$('.langs_list a').on('click',function(){
		$('form#change_lang #lang').val($(this).attr('data-lang'));
		$('form#change_lang').submit();
		return false;
	});
});