<?php
class translation {
	var $projectID;
	var $token;
	var $lang;
	var $userLang;
	var $defaultLang;
	var $translations;
	private $langList;
	private $url = 'https://api.lokalise.co/api/';

	function __construct($projectID,$token,$lang){
		$currentLocal = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2);
		$lang = $lang != false ? $lang : $currentLocal;
		$this->projectID 	= $projectID;
		$this->token 		= $token;
		$this->set_lang($lang);
		$this->set_langList();
		$this->set_texts();
	}

	function get_langsMenu(){
		$tpl 	= '<div class="langs"><a href="#" class="current">{current}</a><ul class="langs_list">[listado]</ul></div>';
		$item 	= '<li class="{class}"><a href="#" data-lang="{langID}">{langText}</a>';
		$html 	= '';

		if(count($this->langList) >0){
			foreach($this->langList as $key=>$value){
				$class 		= strtolower($value->iso);
				$changes 	= array(
					'{class}' 		=> $class,
					'{langID}' 		=> $value->iso,
					'{langText}' 	=> $this->translateText('lang_'.$class),
				);
				$html.= strtr($item,$changes);
			}
		}

		if($html != ''){
			$changes = array(
				'{current}' 	=> strtoupper($this->lang),
				'[listado]' 	=> $html,
			);
			return strtr($tpl,$changes);
		}
	}

	function get_text(){
		return $this->translations;
	}

	function get_lang(){
		return $this->lang;
	}

	private function set_lang($lang) {
		//$this->userLang		= explode("_", Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']));
		$this->userLang		= $this->defaultLang;
		$this->userLang		= $this->userLang[0];
		$this->lang 		= $lang ? $lang : $this->userLang;
	}

	private function set_langList(){
		$serviceUrl = $this->url."language/list?api_token=".$this->token.'&id='.$this->projectID;
		$data = $this->curl_call($serviceUrl,"GET");

		if($data['status']=='OK'){
			$listado 				= json_decode($data['data']);
			$actives 				= array();
			$this->langList 		= array();
			foreach($listado->languages as $key=>$value){
				if($value->is_default == 1){
					$this->defaultLang = $value->iso;
				}
				if($value->words != '0'){
					$changes 	= array(
						'{class}' 		=> strtolower($value->iso),
						'{langID}' 		=> $value->iso,
						'{langText}' 	=> $value->name,
					);
					$actives[] 	= strtolower($value->iso);
					$this->langList[] 	= $value;
				}
			}
			$this->lang = in_array(strtolower($this->lang), $actives) ? $this->lang : $this->defaultLang;
		} else {
			//TODO cuando no este disponible el WS de idiomas
		}
	}

	private function set_texts(){
		$serviceUrl = $this->url."string/list";
		$data = $this->curl_call($serviceUrl,"POST","api_token=".$this->token."&id=".$this->projectID."&langs=['".$this->lang."']&platform_mask=16");

		if($data['status']=='OK'){
			$this->translations = array();
			$textos = (array) json_decode($data['data'])->strings;
			foreach($textos[$this->lang] as $key=>$value){
				$this->translations[$value->key] = $value->translation;
			}
		} else {
			//TODO cuando no este disponible el WS de strings	
		}	
	}

	private function translateText($tag){
		return isset($this->translations[$tag]) ? $this->translations[$tag] : $tag;
	}

	private function curl_call($service,$type,$post_params = false){
		$curl 		= curl_init();

		$params 						= array(
			CURLOPT_URL 				=> $service,
			CURLOPT_RETURNTRANSFER 		=> true,
			CURLOPT_ENCODING 			=> "",
			CURLOPT_MAXREDIRS 			=> 10,
			CURLOPT_TIMEOUT 			=> 30,
			CURLOPT_HTTP_VERSION 		=> CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST 		=> $type,
			CURLOPT_HTTPHEADER 			=> array(
				"Cache-Control: no-cache",
			),
		);
		if($type == "POST"){
			$params[CURLOPT_POSTFIELDS] 	= $post_params;
			$params[CURLOPT_HTTPHEADER][] 	= "Content-Type: application/x-www-form-urlencoded";
		}

		curl_setopt_array($curl, $params);

		$response 		= curl_exec($curl);
		$err 			= curl_error($curl);

		curl_close($curl);
			


		if ($err) {
			return array('status'=>'KO','data'=>$err);
		} else {
			return array('status'=>'OK','data'=>$response);
		}
	}
}