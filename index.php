<?php include_once('function.php'); ?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>" xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ZANKYOU</title>
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&subset=latin-ext" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>assets/css/jquery.fullPage.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>assets/css/style.css<?php echo '?v='.$v; ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>assets/css/magnific-popup.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $url; ?>assets/js/jquery.fullPage.js"></script>
	<script type="text/javascript" src="<?php echo $url; ?>assets/js/countUp.js"></script>
	<script type="text/javascript" src="<?php echo $url; ?>assets/js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="<?php echo $url; ?>assets/js/main.js<?php echo '?v='.$v; ?>"></script>
</head>
<body class="lang_<?php echo $lang; ?>">
	<div id="topnav">
		<a href="#" class="mobile_menu"><?php echo $tle["textMenu"]; ?></a>
		<ul id="menu">
		    <li data-menuanchor="about" class="active"><a href="<?php echo $url; ?>#about"><?php echo $tle["menu_about"]; ?></a></li>
		    <li data-menuanchor="spain"><a href="<?php echo $url; ?>#spain"><?php echo $tle["menu_spain"]; ?></a></li>
		    <li data-menuanchor="foryou"><a href="<?php echo $url; ?>#foryou"><?php echo $tle["menu_foryou"]; ?></a></li>
		    <li data-menuanchor="tools"><a href="<?php echo $url; ?>#tools"><?php echo $tle["menu_tools"]; ?></a></li>
		    <li><a href="https://www.zankyou.es/empresas" target="_blank"><?php echo $tle["menu_contact"]; ?></a></li>
		</ul>
		<ul class="social">
			<li class="facebook"><a href="https://www.facebook.com/ZankyouEspana/" target="_blank"><?php echo $tle["facebookShare"]; ?></a></li>
			<li class="instagram"><a href="https://www.instagram.com/zankyou_bodas/" target="_blank"><?php echo $tle["instagramShare"]; ?></a></li>
		</ul>
	</div>

	<?php echo $menu; ?>

	<div id="fullpage">
		<div class="section section_home" id="section0">
			<video id="myVideo" data-autoplay muted playsinline poster="<?php echo $url; ?>assets/images/zankyou_v2.gif">
				<source src="<?php echo $url; ?>assets/images/zankyou_v2.mp4" type="video/mp4">
				<source src="<?php echo $url; ?>assets/images/zankyou_v2.webm" type="video/webm">
			</video>
		</div>
		<div class="section section_about" id="section1">
			<div class="wrap">
				<div class="layer">
					<h2><?php echo $tle["about_title"]; ?></h2>
					<p><?php echo $tle["about_text"]; ?></p>
					<a href="#" class="btn show_video" data-video="<?php echo $tle["spot_video"]; ?>"><?php echo $tle["about_btn"]; ?></a>
				</div>
			</div>
		</div>
		<div class="section section_spain" id="section2">		
			<div class="slide" id="spain_main">
				<div class="wrap">
		            <div class="layer">
						<h2><?php echo $tle["spain_title"]; ?></h2>
						<p><?php echo $tle["spain_text"]; ?></p>
						<a href="#" class="btn show_video" data-video="<?php echo $tle["zankyou_video"]; ?>"><?php echo $tle["about_btn"]; ?></a>
						<a href="<?php echo $url; ?>#spain/data" class="btn"><?php echo $tle["spain_btn"]; ?></a>
					</div>
				</div>
	        </div>
	        <div class="slide" id="spain_data" data-anchor="data">
	        	<div class="wrap">
		            <div class="layer">
						<div class="counter" id="countitem_01">
							<div id="counter1" class="item" data-max="780000">0</div> <?php echo $tle["data_counter_text_01"]; ?>
						</div>
						<div class="counter" id="countitem_02">
							<div id="counter2" class="item" class="counter" data-max="7000">0</div> <?php echo $tle["data_counter_text_02"]; ?>
						</div>
						<div class="counter" id="countitem_03">
							<div id="counter3" class="item" class="counter" data-max="38000">0</div> <?php echo $tle["data_counter_text_03"]; ?>
						</div>
						<a href="<?php echo $url; ?>#spain" class="btn btn_close">Cerrar</a>
					</div>
				</div>
	        </div>
		</div>
		<div class="section section_foryou" id="section3"><!-- test -->
			<div class="wrap">
				<div class="layer">
					<h2><?php echo $tle["foryou_title"]; ?></h2>
					<p><?php echo $tle["foryou_text"]; ?></p>
					<a href="<?php echo $url; ?>#tools" class="btn btn_tools"><?php echo $tle["see_tools_btn"]; ?></a>
					<a href="https://www.zankyou.es/empresas" target="_blank" class="btn btn_contact"><?php echo $tle["more_info_btn"]; ?></a> 
				</div>
			</div>
		</div>
		<div class="section section_tools" id="section4">
			<div class="slide" id="tools_home">
				<div class="wrap">
					<div class="layer">
						<h2><?php echo $tle["tools_title"]; ?></h2>
						<p><?php echo $tle["tools_text"]; ?></p>
						<a href="<?php echo $url; ?>#tools/control-panel" class="btn"><?php echo $tle["control_panel_btn"]; ?></a>
						<a href="<?php echo $url; ?>#tools/more-tools" class="btn"><?php echo $tle["more_tools_btn"]; ?></a>
					</div>
				</div>
	        </div>
	        <div class="slide subpage" id="tools_control_panel" data-anchor="control-panel">
				<div class="wrap">
					<div class="layer">
						<h3><?php echo $tle["control_panel_title"]; ?></h3>
						<a class="popup" href="<?php echo $url; ?>assets/images/controlpanel_01.jpg">
							<img src="<?php echo $url; ?>assets/images/controlpanel_01_thumb.jpg" alt="<?php echo $tle["tools_title"]; ?>">
						</a>
						<p><?php echo $tle["control_panel_text"]; ?></p>
					</div>
				</div>
				<a href="<?php echo $url; ?>#tools" class="btn btn_close">Cerrar</a>
	        </div>
	        <div class="slide subpage" id="tools_extra" data-anchor="more-tools">
				<div class="wrap">
					<div class="layer">
						<ul class="other_tools">
							<li class="item_01 active">
								<a href="<?php echo $url; ?>assets/images/clasificados_big.jpg" class="popup">
									<img src="<?php echo $url; ?>assets/images/clasificados_thumb.jpg" alt="">
									<strong><?php echo $tle["more_tools01_title"]; ?></strong>
								</a>
							</li>
							<li class="item_02">
								<a href="<?php echo $url; ?>assets/images/magazine_big.jpg" class="popup">
									<img src="<?php echo $url; ?>assets/images/magazine_thumb.jpg" alt="">
									<strong><?php echo $tle["more_tools02_title"]; ?></strong>
								</a>
							</li>
							<li class="item_03">
								<a href="<?php echo $url; ?>assets/images/events_big.jpg" class="popup">
									<img src="<?php echo $url; ?>assets/images/events_thumb.jpg" alt="">
									<strong><?php echo $tle["more_tools03_title"]; ?></strong>
								</a>
							</li>
							<li class="item_04">
								<a href="<?php echo $url; ?>assets/images/chat_big.jpg" class="popup">
									<img src="<?php echo $url; ?>assets/images/chat_thumb.jpg" alt="">
									<strong><?php echo $tle["more_tools04_title"]; ?></strong>
								</a>
							</li>
							<li class="item_05">
								<a href="<?php echo $url; ?>assets/images/app_big.jpg" class="popup">
									<img src="<?php echo $url; ?>assets/images/app_thumb.jpg" alt="">
									<strong><?php echo $tle["more_tools05_title"]; ?></strong>
								</a>
							</li>
							<li class="item_06">
								<a href="<?php echo $url; ?>assets/images/stats_big.jpg" class="popup">
									<img src="<?php echo $url; ?>assets/images/stats_thumb.jpg" alt="">
									<strong><?php echo $tle["more_tools06_title"]; ?></strong>
								</a>
							</li>
						</ul>
						<div class="nav_pagination">
							<a href="#" data-item="item_01" class="active">1</a>
							<a href="#" data-item="item_02">2</a>
							<a href="#" data-item="item_03">3</a>
							<a href="#" data-item="item_04">4</a>
							<a href="#" data-item="item_05">5</a>
							<a href="#" data-item="item_06">6</a>
						</div>
						<h3><?php echo $tle["more_tools_title"]; ?></h3>
						<p><?php echo $tle["more_tools_text"]; ?></p>
					</div>
				</div>
				<a href="<?php echo $url; ?>#tools" class="btn btn_close">Cerrar</a>
	        </div>
		</div>
	</div>
	<div class="modal">
		<a href="#" class="close">cerrar</a>
		<div class="video"></div>
	</div>
	<form id="change_lang" action="" method="post">
		<input type="hidden" id="lang" name="lang" value="<?php echo $lang; ?>">
	</form>
</body>
</html>